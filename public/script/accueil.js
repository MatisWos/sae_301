//Changement de page
document.querySelector('.svg-icon-avatar').addEventListener('click', function() {
    window.location.href = '?route=profil';
});

// document.querySelector('.svg-photo').addEventListener('click', function() {
//     window.location.href = '?route=ajoutPhoto';
// });

try {
  // On exécute un bloc de code qui peut générer une erreur
  document.querySelector('.svg-panier').addEventListener('click', function() {
    window.location.href = '?route=panier';
  });
} catch (error) {
  // On affiche l'erreur dans la console
  console.log(error);
}

try {
  // On exécute un bloc de code qui peut générer une erreur
  document.querySelector('.svg-photo').addEventListener('click', function() {
    window.location.href = '?route=ajoutPhoto';
  });
} catch (error) {
  // On affiche l'erreur dans la console
  console.log(error);
}


// CAROUSEL
// Le carousel doit évoluer au clic
const flecheG = $('section>div>svg:nth-of-type(1)');
const flecheD = $('section>div>svg:nth-of-type(2)');
const image = $('section>div>div>img');
const txt = $('section>article>div>p');


const infos = [ // Liste du contenu textuel à faire apparaître 
  'Burger géant',
  'Ramen',
  'Pizz\'incroyable'
];

image.slice(1).hide(); //On cache toutes les images sauf la première 

let x = 1; //x sera la variable nous permettant de connaitre notre position dans la liste d'image
flecheD.on('click', () => { // au clic de la flèche de droite
  $('section>div>div>img:nth-of-type(' + x + ')').fadeOut(); //Faire disparaître l'imagine active avec une animation de fondu de sortie
  x += 1;//Mise a jour de notre position
  if (x == 4) { //on vérifie si nous ne sommes pas sortis de la liste
    x = 1; // si c'est la cas on revient à notre première image
  }
  $('section>div>div>img:nth-of-type(' + x + ')').fadeIn(); // Apparition de la nouvelle image avec un effet de fondu d'entrée
  txt.text(infos[x - 1]); //Mise a jour du texte qui acconpagne le carousel
});

flecheG.on('click', () => { // au clic de la flèche de gauche
  $('section>div>div>img:nth-of-type(' + x + ')').fadeOut();//Faire disparaître l'imagine active avec une animation de fondu de sortie
  x -= 1;//Mise a jour de notre position
  if (x == 0) { //on vérifie si nous ne sommes pas sortis de la liste
    x = 3;// si c'est la cas on revient à notre dernière image
  }
  $('section>div>div>img:nth-of-type(' + x + ')').fadeIn(); // Apparition de la nouvelle image avec un effet de fondu d'entrée
  txt.text(infos[x - 1]); //Mise a jour du texte qui acconpagne le carousel
});

//SELECTION MENU


var menus = document.querySelectorAll('.Menu');

for (var i = 0; i < menus.length; i++) {
  menus[i].addEventListener('click', function() {
    const divCliquer = event.currentTarget;
    const menuElement = document.createElement('menu');
    
    const titre = divCliquer.querySelector('h3');
    const source = divCliquer.querySelector('img');
    const texte = divCliquer.querySelector('p:nth-of-type(3)');

    // // Créer un formulaire
    // const form = document.createElement("form");
    // form.action = "application/vues/page_menu.php";
    // form.method = "POST";

    // // Ajouter des champs au formulaire
    // const titreInput = document.createElement("input");
    // titreInput.type = "text";
    // titreInput.name = "titre";
    // titreInput.value = titre.textContent;

    // const sourceInput = document.createElement("input");
    // sourceInput.type = "text";
    // sourceInput.name = "source";
    // sourceInput.value = source.src;

    // const texteInput = document.createElement("input");
    // texteInput.type = "text";
    // texteInput.name = "texte";
    // texteInput.value = texte.textContent;

    // form.appendChild(titreInput);
    // form.appendChild(sourceInput);
    // form.appendChild(texteInput);

    // // Soumettre le formulaire
    // form.submit();

    window.location.href = '?route=menu';
  });
}



// ---------------------------------------------étoiles---------------------------------------------

/* eslint-disable require-jsdoc */
const ratingInputs = document.querySelectorAll('.rating input[type=\'range\']');
const starsList = document.querySelectorAll('.rating .stars');
const selectedTextList = document.querySelectorAll('.rating .selected-text');

ratingInputs.forEach(function(ratingInput, index) {
  const stars = starsList[index].querySelectorAll('span');
  const selectedText = selectedTextList[index];

  ratingInput.addEventListener('input', function() {
    const value = this.value;
    updateStars(stars, value, selectedText);
  });

  stars.forEach(function(star, starIndex) {
    star.addEventListener('click', function() {
      const value = stars.length - starIndex;
      ratingInput.value = value;
      updateStars(stars, value, selectedText);
    });
  });
});

function updateStars(stars, value, selectedText) {
  for (let i = 0; i < stars.length; i++) {
    if (i < value) {
      stars[stars.length - 1 - i].classList.add('selected');
    } else {
      stars[stars.length - 1 - i].classList.remove('selected');
    }
  }
  selectedText.textContent = value + ' étoile(s) sélectionnée(s)';
}