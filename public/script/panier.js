document.getElementById('SVG_1').addEventListener('click', function () {
    window.history.back();
});


//-------------------------------CARTE (gestion de la carte + localisation + filtre des cinémas proches de nous)-------------------------------------------------------

let svg = `<?xml version="1.0" ?><svg style="enable-background:new 0 0 512 512;" version="1.1" viewBox="0 0 512 512" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="_x32_3_x2C__Location_x2C__map_x2C__pin_x2C__mark_x2C__navigation"><g><path d="M256,32.7c85.36,0,154.53,69.2,154.53,154.53c0,64-114.82,220.49-154.53,292.07    c-38.07-68.7-154.53-231.83-154.53-292.07C101.47,101.9,170.67,32.7,256,32.7z M325.07,181.41c0-38.03-31.02-69.05-69.07-69.05    c-38.02,0-69.04,31.02-69.04,69.05c0,38.02,31.02,69.04,69.04,69.04C294.05,250.45,325.07,219.43,325.07,181.41z" style="fill:#E74C3C;"/><path d="M256,489.3c-0.001,0-0.001,0-0.002,0c-3.635-0.001-6.982-1.974-8.745-5.153c-10.098-18.222-25.631-42.934-43.617-71.547    C153.663,333.102,91.47,234.162,91.47,187.23C91.47,96.508,165.278,22.7,256,22.7s164.53,73.808,164.53,164.53    c0,49.378-58.682,141.696-110.456,223.147c-17.792,27.99-34.597,54.428-45.33,73.774C262.981,487.328,259.634,489.3,256,489.3z     M256,42.7c-79.694,0-144.53,64.836-144.53,144.53c0,41.168,63.056,141.481,109.098,214.726    c13.602,21.64,25.813,41.065,35.442,57.316c10.349-17.405,23.477-38.059,37.186-59.625    C338.493,328.386,400.53,230.79,400.53,187.23C400.53,107.536,335.694,42.7,256,42.7z"/><path d="M256,260.45c-43.583,0-79.04-35.457-79.04-79.04c0-43.588,35.457-79.05,79.04-79.05c43.599,0,79.07,35.462,79.07,79.05    C335.07,224.993,299.599,260.45,256,260.45z M256,122.36c-32.555,0-59.04,26.49-59.04,59.05c0,32.555,26.485,59.04,59.04,59.04    c32.571,0,59.07-26.485,59.07-59.04C315.07,148.85,288.571,122.36,256,122.36z"/></g></g><g id="Layer_1"/></svg>`;
let encodedSvg = btoa(svg);

let svg2 = `<svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 329.06 466.6"><defs><style>.cls-1{fill:#423ee5;}</style></defs><title>Dominique</title><path class="cls-1" d="M256,32.7A154.53,154.53,0,0,1,410.53,187.23c0,64-114.82,220.49-154.53,292.07-38.07-68.7-154.53-231.83-154.53-292.07A154.55,154.55,0,0,1,256,32.7Zm69.07,148.71a69.06,69.06,0,1,0-69.07,69A69.19,69.19,0,0,0,325.07,181.41Z" transform="translate(-91.47 -22.7)"/><path d="M256,489.3h0a10,10,0,0,1-8.75-5.15c-10.1-18.23-25.63-42.94-43.61-71.55-50-79.5-112.17-178.44-112.17-225.37C91.47,96.51,165.28,22.7,256,22.7S420.53,96.51,420.53,187.23c0,49.38-58.68,141.7-110.46,223.15-17.79,28-34.59,54.43-45.33,73.77A10,10,0,0,1,256,489.3Zm0-446.6c-79.69,0-144.53,64.84-144.53,144.53,0,41.17,63.06,141.48,109.1,214.73C234.17,423.6,246.38,443,256,459.27c10.35-17.4,23.48-38.06,37.19-59.62,45.29-71.26,107.33-168.86,107.33-212.42C400.53,107.54,335.69,42.7,256,42.7Z" transform="translate(-91.47 -22.7)"/><path d="M256,260.45a79,79,0,1,1,79.07-79A79.13,79.13,0,0,1,256,260.45Zm0-138.09a59,59,0,1,0,59.07,59.05A59.12,59.12,0,0,0,256,122.36Z" transform="translate(-91.47 -22.7)"/></svg>`;
let encodedSvg2 = btoa(svg2);

let maisonMark = L.icon({
    iconUrl: 'data:image/svg+xml;base64,' + encodedSvg,
    iconSize: [40, 60],
});

let dominique = L.icon({
    iconUrl: 'data:image/svg+xml;base64,' + encodedSvg2,
    iconSize: [25, 35],
});

let map = L.map('carte').setView([47.2608333, 2.4188888888888886], 5); //création de la carte centrée sur la France
L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map); // Ajout du fond de carte

// Ajout d'un marqueur à la position spécifiée
let lat = 48.1058286500006;
let lon = -1.67879094998914;
let marker = L.marker([lat, lon], {
    icon: dominique
}).addTo(map);
marker.bindPopup("Click & collect de Dominique").openPopup();

// Appelée si récupération des coordonnées réussie
function positionSucces(position) {
    // Injection du résultat dans du texte
    const boite = document.querySelector('main>section>div');
    const lat = Math.round(1000 * position.coords.latitude) / 1000;
    const long = Math.round(1000 * position.coords.longitude) / 1000;

    let marker = L.marker([lat, long], {
        icon: maisonMark
    }).addTo(map);
    marker.bindPopup("Vous êtes ici").openPopup();
    boite.classList.toggle("effect-div");
    map.flyTo([lat, long], 13); // zoom de la carte sur la position de l'utilisateur
    setTimeout(() => {
        boite.classList.toggle("effect-div");
    }, 2000);
    localise(48.1058286500006, -1.67879094998914); //Localisation fictive de Dominique

}

// Appelée si échec de récuparation des coordonnées
function positionErreur(erreurPosition) {
    // Cas d'usage du switch !
    let natureErreur;
    switch (erreurPosition.code) {
        case erreurPosition.TIMEOUT:
            // Attention, durée par défaut de récupération des coordonnées infini
            natureErreur = "La géolocalisation prends trop de temps...";
            break;
        case erreurPosition.PERMISSION_DENIED:
            natureErreur = "Vous n'avez pas autorisé la géolocalisation.";
            break;
        case erreurPosition.POSITION_UNAVAILABLE:
            natureErreur = "Votre position n'a pu être déterminée.";
            break;
        default:
            natureErreur = "Une erreur inattendue s'est produite.";
    }
    // Injection du texte
    console.log(natureErreur);
}

// Récupération des coordonnées au clic sur le bouton
$("body>main>section:first-of-type>div:first-of-type>button").click(function () {
    // Support de la géolocalisation
    if ("geolocation" in navigator) {
        // Support = exécution du callback selon le résultat
        navigator.geolocation.getCurrentPosition(positionSucces, positionErreur, {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 30000
        });
    } else {
        console.log("La géolocalisation n'est pas supportée par votre navigateur");
    }
});

// Créez un élément <p> et stockez-le dans une variable
let p = document.createElement('p');

// Ajoutez l'élément <p> à l'endroit spécifié dans le document
document.querySelector('body>main>section:first-of-type>div:first-of-type').appendChild(p);


function localise(lat2, lon2) {
    let lat1, lon1;

    // Récupération des coordonnées actuelles
    navigator.geolocation.getCurrentPosition((position) => {
        lat1 = position.coords.latitude;
        lon1 = position.coords.longitude;

        // Conversion des degrés en radians
        lat1 = lat1 * (Math.PI / 180);
        lon1 = lon1 * (Math.PI / 180);
        lat2 = lat2 * (Math.PI / 180);
        lon2 = lon2 * (Math.PI / 180);

        // Calcul de la différence de coordonnées
        let dlon = lon2 - lon1;
        let dlat = lat2 - lat1;

        // Formule de Haversine
        let a = Math.pow(Math.sin(dlat / 2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon / 2), 2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        // Rayon de la Terre en kilomètres
        let R = 6371;

        // Calcul de la distance
        let distance = R * c;

        // Arrondi de la distance à la dizaine la plus proche
        distance = parseFloat(distance.toFixed(2));

        // Mise à jour du contenu de l'élément <p>
        p.textContent = `La distance entre votre position et le point donné est de ${distance} kilomètres.`;
    });
}