<?php

// lancement de la session
session_start();

// définition de l'URL de base pour les redirections
define('baseURL', 'http://localhost/SAE301/');

// Si la route est configurée lors de l'appel à cette page, on appelle le bon contrôleur
if (isset($_GET['route'])) {
    switch ($_GET['route']) {
        case 'profil':
            require('application/controleurs/profil.php');
            break;
        case 'profil-perso-creation':
            require('application/controleurs/profil-perso-creation.php');
            break;
        case 'profil-perso-connexion':
            require('application/controleurs/profil-perso-connexion.php');
            break;
        case 'profil-perso-deconnexion':
            require('application/modeles/profil-admin.class.php');
            $m = new ProfilAdmin("root", "");
            $m->deconnexion();
            break;
        case 'ajoutPhoto':
            require('application/controleurs/gestionPhotos.php');
            ajoutPhoto();
            break;
        case 'menu':
            require('application/controleurs/menu.php');
            break;
        case 'panier':
            require('application/controleurs/panier.php');
            break;
        case 'votes':
            require('application/controleurs/gestionVotes.php');
            break;
    }
} else {
    // sinon on appelle le contrôleur par défaut
    require("application/controleurs/accueil.php");
}
?>