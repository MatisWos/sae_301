<?php
//si pas de compte afficher vue page_profil_perso
if (empty($_POST)) {
    require("application/vues/page_profil_perso_creation.php");
//Afficher profil existant -> modeles de profil.class.php
} else {
    require 'application/modeles/profil.class.php';
    require 'application/modeles/profil-admin.class.php';
    $nom = $_POST['Nom'];
    $prenom = $_POST['Prenom'];
    $telephone = $_POST['Telephone'];
    $adresseMail = $_POST['Adresse_Mail'];
    $adressePostale = $_POST['Adresse_Postale'];
    $motDePasse = $_POST['motDePasse'];
    $situation = $_POST['situation'];
    $texte='';
    $m = new ProfilAdmin("root", "");
    if ($m -> existeProfil($adresseMail)  == false) {
        // Créer un nouveau profil
        $p = new Profil(array('nom' => $nom, 'prenom' => $prenom, 'telephone' => $telephone, 'adresseMail' => $adresseMail, 'adressePostale' => $adressePostale, 'motDePasse' => $motDePasse, 'situation' => $situation));
        echo($situation);
        //Ajout a la BD
        $m-> add($p);
        // Effectuer la redirection vers la page d'accueil
        $_SESSION['adresseMail'] = $adresseMail; 
        header("Location: index.php");
        exit();
    }
    else{     
        $texte = "<p>Votre adresse mail est déjà enregistrer dans la base de donnée</p>";
    }
    echo ($texte);
}
?>