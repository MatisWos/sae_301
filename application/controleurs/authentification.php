<?php

function inscription()
{
    if (empty($_POST)) {
        require("application/vues/page_inscription.php");
    } else {
        require("application/modeles/utilisateurs.php");
        $pseudo = $_POST["pseudo"];
        $mail = $_POST["mail"];
        $password1 = $_POST["password1"];
        $password2 = $_POST["password2"];
        if ($password1 != $password2) {
            $_SESSION["error"] = "<p class='colorerror'>/ ! \ Les mots de passe sont différents</p>";
            echo $_SESSION['error'];
            header('Location:' . baseURL . 'index.php?route=inscription');
           
            
        
        } elseif (existe_utilisateur($pseudo, $mail)) {
            $_SESSION["error"] = "Ce pseudo ou cette adresse sont déjà utilisés";
            echo "$_SESSION[error]";
            header('Location:' . baseURL . 'index.php?route=inscription');
        } else {
            inscription_utilisateur($pseudo, $mail, $password1);
            header("Location:" . baseURL . "index.php?");
        }
    }
}

function connexion()
{
    if (empty($_POST)) {
        require("application/vues/page_connexion.php");
    } else {
        require("application/modeles/utilisateurs.php");
        $pseudo = $_POST["pseudo"];
        $password = $_POST["password"];
        if (existe_utilisateur2($pseudo, $password)) {
            $_SESSION["pseudo"] = $pseudo;
            header("Location:" . baseURL. "index.php");
        } else {
            $_SESSION["error"] = "<p class='colorerror'>/ ! \ Pseudo ou mot de passe incorrect</p>";
            echo "$_SESSION[error]";
            // echo "<p class='colortext'>" . $_SESSION["error"] . "</p>";
            // echo "vcserfe";
            header('Location:' . baseURL . 'index.php?route=connexion');
        }
    }
}

function deconnexion() {
    unset($_SESSION["pseudo"]);
    header("Location:" . baseURL . "index.php?");
}