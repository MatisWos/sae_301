<?php
require 'application/modeles/photos.class.php';
require 'application/modeles/profil-admin.class.php';

function ajoutPhoto()
{
    // Vérifie si le formulaire n'a pas été soumis
    if (empty($_POST)) {
        require('application/vues/ajoutPhoto.php');
    } else {
        // Récupère les données du formulaire
        // $adresseMail = $_SESSION['dominique.legoat@gmail.com'];
        $adresseMail = $_SESSION['adresseMail'];
        $description = $_POST['description'];
        $date = date('Y-m-d H:i:s');
        $titre = $_POST['titre'];
        $nomTmp = $_FILES['photo']['tmp_name'];

    
        $m = new ProfilAdmin("root", "");
        // Appelle la fonction pour l'envoi de la photo
        $m -> envoiePhoto($adresseMail, $description, $date, $titre, $nomTmp);

        // Effectue la redirection vers la page d'accueil
        header("Location: index.php");
        exit();
    }
}

function affichePhoto($id, $adresseMail, $description, $chemin, $date, $titre)
{
    // Affiche les informations de la photo selon le format souhaité
    echo "<div class='Menu'>";
    echo "<h3>$titre</h3>";
    echo "<img src='$chemin' alt='$titre'>";
    echo "<p><!--Adresse Mail :--> $adresseMail</p>";
    echo "<p><!--Date :--> $date</p>";
    echo "<p><!--Description :--> $description</p>";
    
    echo "</div>";

    echo "<div>";
    if (isset($_SESSION['adresseMail'])) {
        // Affiche le formulaire de vote si l'utilisateur est connecté
        echo "<form action='index.php?route=votes' method='POST'>";
        echo "<input type='hidden' name='id_photo' value='$id'>";
        echo "<div class='rating'>";
        echo "<label for='note'>Vote : </label>";
        echo "<input type='range' name='note' id='note' value='0' min='1' max='5' step='1' />";
        echo "<div class='stars'>";
        echo "<span></span>";
        echo "<span></span>";
        echo "<span></span>";
        echo "<span></span>";
        echo "<span></span>";
        echo "</div>";
        echo "<p class='selected-text'>0 étoile(s) sélectionnée(s)</p>";
        echo "</div>";
        echo "<input type='submit' value='Voter'>";
        echo "</form>";
        echo "La note de ce menu est : ". (new ProfilAdmin("root", "")) -> calculMoyenneVote($id).".";
    }
    echo "</div>";
}



function affichePhotos($liste) {
    // Parcourez le tableau associatif et affichez chaque photo en utilisant la fonction affichePhoto()
    $vide='';
    foreach ($liste as $photo) {
        $vide = $vide.affichePhoto($photo['id_photo'],$photo['mail_photo'], $photo['description_photo'], $photo['chemin_photo'], $photo['date_photo'], $photo['titre_photo']);
    }
    return $vide;
}
?>
