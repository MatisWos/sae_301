<?php
if (empty($_SESSION["adresseMail"])) {
  // L'utilisateur n'est pas connecté
  // Rediriger vers la page d'accueil
  require("application/vues/page_accueil_profil.php");
} else {
  require("application/vues/page_menu.php");
}
?>