<?php
// Inclusion du modèle des votes
require 'application/modeles/votes.class.php';
require('application/modeles/profil-admin.class.php');

if (!empty($_POST)) {
    // Récupérer les données du formulaire
    $mail_vote = $_SESSION['adresseMail'];
    $id_photo = isset($_POST['id_photo']) ? $_POST['id_photo'] : null;
    $note = isset($_POST['note']) ? $_POST['note'] : null;

    // Appeler la fonction d'importation de la photo du modèle
    $m = new ProfilAdmin("root", "");
    $m -> ajoutVoteBDD($id_photo, $mail_vote, $note);
        
    // Effectuer la redirection vers la page d'accueil
    header("Location: index.php");
    exit();
}

?>