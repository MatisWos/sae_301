<?php
//si pas de compte afficher vue page_profil_perso
if (empty($_POST)) {
    require("application/vues/page_profil_perso_connexion.php");
//Afficher profil existant -> modeles de profil.class.php
} else {
    require 'application/modeles/profil.class.php';
    require 'application/modeles/profil-admin.class.php';
    $adresseMail = $_POST['Adresse_Mail'];
    $motDePasse = $_POST['motDePasse'];
    $texte='';
    $m = new ProfilAdmin("root", "");
    // on vérifie que tous les champs sont remplis
    if (empty($adresseMail) || empty($motDePasse)) {
        // on vide le tableau $_POST pour éviter une boucle de redirection infinie
        $_POST = [];
        $_SESSION['error'] = "Merci de renseigner tous les champs";
        header('Location: ' . baseURL . 'index.php?route=profil-perso-connexion');
    }

    // Vérification de l'authentification de l'utilisateur à l'aide d'une fonction du modèle
    if ($m -> connexion($adresseMail, $motDePasse)) {
        $_SESSION['adresseMail'] = $adresseMail;
        header("Location: index.php");
    } else {
        $_SESSION['error'] = "Identifiants ou mot de passe incorrects";
        $_POST = [];
        header('Location: ' . baseURL . 'index.php?route=profil-perso-connexion');
    }
}
?>