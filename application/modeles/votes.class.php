<?php

class Vote
{

    /*---------------------------------------------------Attributs---------------------------------------------------*/
    protected $_photoVote;
    protected $_mailVote;
    protected $_valeurVote;
    /*---------------------------------------------------Methodes---------------------------------------------------*/

    public function __construct($t)
    {
        $this->hydrate($t);
    }

    public function hydrate(array $donnees)
    {
        foreach ($donnees as $k => $v) {
            $f = "set" . ucfirst($k);
            if (method_exists($this, $f))
                $this->$f($v);
        }
    }


    /*---------------------------------------------------Getters---------------------------------------------------*/

    public function getPhotoVote()
    {
        return $this->_photoVote;
    }

    public function getMailVote()
    {
        return $this->_mailVote;
    }

    public function getValeurVote()
    {
        return $this->_valeurVote;
    }

    /*---------------------------------------------------Setters---------------------------------------------------*/

    public function setPhotoVote(int $photoVote)
    {
        $this->_photoVote = $photoVote;
    }

    public function setMailVote(string $mailVote)
    {
        if (empty($mailVote)) {
            throw new Exception("L' adresse Mail ne peut pas être vide");
        }
        $this->_mailVote = $mailVote;
    }

    public function setValeurVote(float $valeurVote)
    {
        if (empty($valeurVote)) {
            throw new Exception("Le vote ne peut pas être vide");
        }
        $this->_valeurVote = $valeurVote;
    }
}


?>