<?php
class Profil {

    /***************************************************Attributs***************************************************/
    protected $_id;
    protected $_nom;
    protected $_prenom;
    protected $_telephone;
    protected $_adresseMail;
    protected $_adressePostale;
    protected $_motDePasse;
    protected $_situation;
    /***************************************************Constantes***************************************************/

    /***************************************************Methodes***************************************************/   
    // Méthodes

    public function __construct($t){
        foreach($t as $k =>$v){
            $f="set".ucfirst($k);
            if(method_exists($this,$f))
            $this->$f($v);
        }
    }

    
    /***************************************************Getters***************************************************/
    public function getId(){
        return $this->_id;
      }
    
    public function getNom() {
        return $this->_nom;
      }

    public function getPrenom() {
        return $this->_prenom;
      }

    public function getTelephone() {
      return $this->_telephone;
    }

    public function getAdresseMail() {
        return $this->_adresseMail;
      }

    public function getAdressePostale() {
        return $this->_adressePostale;
      }

    public function getMotDePasse() {
        return $this->_motDePasse;
      }
      
    public function getSituation() {
        return $this->_situation;
      }
    /***************************************************Setters***************************************************/
    
    public function setId(int $Id) {
        // Vérifier que l'id' est comprise entre 1 et 100
        if ($Id < 0 || $Id >= 100) {
            throw new Exception("L'Id doit être comprise entre 0 et 100");
      }
  
      // Assigner la nouvelle valeur
      $this->_id = $Id;
      }  


    public function setNom(string $nom) {
        // Vérifier que le nom n'est pas vide
        if (empty($nom)) {
        throw new Exception("Le nom ne peut pas être vide");
      }
  
      // Assigner la nouvelle valeur
      $this->_nom = $nom;
      }

    public function setPrenom(string $prenom) {
        // Vérifier que le nom n'est pas vide
        if (empty($prenom)) {
        throw new Exception("Le prénom ne peut pas être vide");
      }
  
      // Assigner la nouvelle valeur
      $this->_prenom = $prenom;
      }
    
      public function setTelephone(int $telephone) {
        // Vérifier que le telephone n'est pas vide
        if (empty($telephone)) {
        throw new Exception("Le numero de téléphone ne peut pas être vide");
      }
  
      // Assigner la nouvelle valeur
      $this->_telephone = $telephone;
      }

      public function setAdresseMail(string $adresseMail) {
        // Vérifier que le telephone n'est pas vide
        if (empty($adresseMail)) {
        throw new Exception("L' adresse Mail ne peut pas être vide");
      }
  
      // Assigner la nouvelle valeur
      $this->_adresseMail = $adresseMail;
      }

      public function setAdressePostale(string $adressePostale) {
        // Vérifier que le telephone n'est pas vide
        if (empty($adressePostale)) {
        throw new Exception("L' adresse Postale ne peut pas être vide");
      }
  
      // Assigner la nouvelle valeur
      $this->_adressePostale = $adressePostale;
      }

      public function setMotDePasse(string $motDePasse) {
        // Vérifier que le telephone n'est pas vide
        if (empty($motDePasse)) {
        throw new Exception("Le mot de passe ne peut pas être vide");
      }
  
      // Assigner la nouvelle valeur
      $this->_motDePasse = $motDePasse;
      }

      public function setSituation(string $situation) {
        // Vérifier que le telephone n'est pas vide
        if (empty($situation)) {
          throw new Exception("Le mot de passe ne peut pas être vide");
      }
  
      // Assigner la nouvelle valeur
      $this->_situation = $situation;
      }
    /***************************************************Hydrate***************************************************/
      public function hydrate()
      {
        if (isset($donnees['ID'])) {
              $this->_id = $donnees['ID'];
            }
  
        if (isset($donnees['Nom'])) {
              $this->_nom = $donnees['Nom'];
            }
  
        if (isset($donnees['Prenom'])) {
              $this->_prenom = $donnees['Prenom'];
            }

        if (isset($donnees['Telephone'])) {
            $this->_telephone = $donnees['Telephone'];
            }

        if (isset($donnees['Adresse_Mail'])) {
            $this->_adresseMail = $donnees['Adresse_Mail'];
            }

        if (isset($donnees['Adresse_Postale'])) {
            $this->_adressePostale = $donnees['Adresse_Postale'];
            }

        if (isset($donnees['MotDePasse'])) {
            $this->_motDePasse = $donnees['motDePasse'];
              }

        if (isset($donnees['Situation'])) {
            $this->_situation = $donnees['situation'];
            }
      }
    
}

?>