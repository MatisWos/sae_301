<?php
require_once 'profil.class.php';

class ProfilAdmin
{

  /***************************************************Attributs***************************************************/
  private $_db;

  /***************************************************Constructeur***************************************************/
  public function __construct($user, $mdp)
  {
    $this->setbdd($user, $mdp);
  }

  public function setbdd($user, $mdp)
  {
    $this->_db = new PDO("mysql:host=localhost;dbname=sae301", $user, $mdp);
  }
  /***************************************************Fonctionnalités***************************************************/

  public function add(Profil $profil)
  {
    // Préparer la requête d'insertion
    $sql = "INSERT INTO profil (nom, prenom, telephone, adresseMail, adressePostale, motDePasse, situation) VALUES (:nom, :prenom, :telephone, :adresseMail, :adressePostale, :motDePasse, :situation)";
    $stmt = $this->_db->prepare($sql);

    // Assigner les valeurs aux paramètres de la requête
    $stmt->bindValue(":nom", $profil->getNom());
    $stmt->bindValue(":prenom", $profil->getPrenom());
    $stmt->bindValue(":telephone", $profil->getTelephone());
    $stmt->bindValue(":adresseMail", $profil->getAdresseMail());
    $stmt->bindValue(":adressePostale", $profil->getAdressePostale());
    $stmt->bindValue(":motDePasse", $profil->getMotDePasse());
    $stmt->bindValue(":situation", $profil->getSituation());

    // Exécuter la requête
    $stmt->execute();

    // Retourner l'identifiant du personnage inséré
    return $this->_db->lastInsertId();
  }

  public function existeProfil($mail)
  {
    $sql = "SELECT * FROM profil WHERE adresseMail = ?";
    $sth = $this->_db->prepare($sql);
    $sth->execute(array($mail));
    $result = $sth->fetch(PDO::FETCH_ASSOC);
    //$_db = null;    
    if ($result == "") {
      return false;
    }
    return (true);
  }

  public function connexion($mail, $motDePasse)
  {
    $sql = "SELECT * FROM profil WHERE adresseMail = ? AND motDePasse = ?";
    $sth = $this->_db->prepare($sql);
    $sth->execute(array($mail, $motDePasse));
    $result = $sth->fetch(PDO::FETCH_ASSOC);
    //$_db = null;    
    if ($result == "") {
      return false;
    }
    return (true);
  }

  public function deconnexion()
  {
    unset($_SESSION['adresseMail']);
    header('Location: ' . baseURL . 'index.php');
  }

  // ------------------------------------Images----------------------------------------------------------------

  public function envoiePhoto($adresseMail, $description, $date, $titre, $nomTmp)
  {
    // Les espaces dans le titre sont remplacés par des underscore
    $titre_chg = str_replace(' ', '_', $titre);
    // Les espaces dans le nom de l'auteur sont remplacés par des underscore
    $adresseMail_chg = str_replace(' ', '_', $adresseMail);
    // La photo est enregistré avec le nom de l'auteur et dutitre
    // /!\ Cela permet de nes pas avoir de problème avec les espaces lorsuq'il s'agit de récupérer les images dans public/medias /!\
    $chemin = 'public/medias/' . $adresseMail_chg . $titre_chg . '.jpg';
    // Stockage de la photo
    move_uploaded_file($nomTmp, $chemin);

    // Insertion des données de l'image dans la base de données
    $stmt = $this->_db->prepare("INSERT INTO photo (mail_photo, description_photo,chemin_photo, date_photo, titre_photo ) VALUES (?, ?, ?, ?, ?)");
    $stmt->execute([$adresseMail, $description, $chemin, $date, $titre]);
  }

  public function getPhotosRecentes()
  {
    // Requête SQL pour récupérer les photos les plus récentes
    $query = "SELECT * FROM photo ORDER BY id_photo DESC LIMIT 3";
    $stmt = $this->_db->prepare($query);
    $stmt->execute();

    // Récupération des résultats de la requête dans un tableau associatif
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return $result;
  }

  // ------------------------------------Votes----------------------------------------------------------------

  // Renvoie null si le vote n'existe pas
  function obtenirVote($id_photo, $mail_vote)
  {
    $sql =  "SELECT valeur_vote FROM vote  WHERE photo_vote = ? AND mail_vote = ?";
    $sth = $this->_db->prepare($sql);

    $sth->execute([$id_photo, $mail_vote]);
    $result = $sth->fetch(PDO::FETCH_ASSOC);
    if (empty($result)) {
      return null;
    }
    return $result['valeur_vote'];
  }

  function ajoutVoteBDD($id_photo, $mail_vote, $note)
  {
    // Si une note est déjà présente pour ce couple photo/utilisateur, on commence par l'effacer
    if ($this->obtenirVote($id_photo, $mail_vote) != null) {
      $sqlDelete = "DELETE FROM vote WHERE photo_vote = ? AND mail_vote = ?";
      $sthDelete = $this->_db->prepare($sqlDelete);
      $sthDelete->execute([$id_photo, $mail_vote]);
    }
    // Ajout du nouveau vote
    $sqlInsert = "INSERT INTO vote (photo_vote, mail_vote, valeur_vote) VALUES (?, ?, ?)";
    $sthInsert = $this->_db->prepare($sqlInsert);
    $sthInsert->execute([$id_photo, $mail_vote, $note]);
  }

  function calculMoyenneVote($id_photo) {

    // Requête SQL pour obtenir la moyenne des notes de la photo sélectionnée
    $sql = "SELECT AVG(valeur_vote) AS moyenne FROM vote WHERE photo_vote = ?";
    $sth = $this->_db->prepare($sql);
    $sth->execute(array($id_photo));
    // Récupération de la moyenne des notes
    $row = $sth->fetch(PDO::FETCH_ASSOC);
    $moyenne = $row['moyenne'];
    // Arrondir la moyenne à deux décimales
    $moyenneArrondie = round($moyenne, 2);
    // Retourner la moyenne
    return $moyenneArrondie;
}

  

  /***************************************************Getters***************************************************/
  public function getDb()
  {
    return $this->_db;
  }
  /***************************************************Setters***************************************************/
  public function setDb($db)
  {
    $this->_db = $db;
  }
}
