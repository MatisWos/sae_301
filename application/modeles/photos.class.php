<?php

class Photo
{

    /*---------------------------------------------------Attributs---------------------------------------------------*/

    protected $_id;
    protected $_adresseMail;
    protected $_description;
    protected $_date;
    protected $_titre;
    protected $_nomTmp;

    /*---------------------------------------------------Methodes---------------------------------------------------*/

    public function __construct($t)
    {
        foreach ($t as $k => $v) {
            $f = "set" . ucfirst($k);
            if (method_exists($this, $f))
                $this->$f($v);
        }
    }
    
    /*---------------------------------------------------Getters---------------------------------------------------*/

    public function getId()
    {
        return $this->_id;
    }

    public function getAdresseMail()
    {
        return $this->_adresseMail;
    }

    public function getDescription()
    {
        return $this->_description;
    }

    public function getDate()
    {
        return $this->_date;
    }

    public function getTitre()
    {
        return $this->_titre;
    }

    public function getNomTmp()
    {
        return $this->_nomTmp;
    }

    /*---------------------------------------------------Setters---------------------------------------------------*/

    public function setId(int $Id)
    {
        // Vérifier que l'id' est comprise entre 1 et 100
        if ($Id < 0 || $Id >= 100) {
            throw new Exception("L'Id doit être comprise entre 0 et 100");
        }

        // Assigner la nouvelle valeur
        $this->_id = $Id;
    }

    public function setAdresseMail(string $adresseMail)
    {
        // Vérifier que le telephone n'est pas vide
        if (empty($adresseMail)) {
            throw new Exception("L' adresse Mail ne peut pas être vide");
        }

        // Assigner la nouvelle valeur
        $this->_adresseMail = $adresseMail;
    }


    public function setDescription(string $description)
    {
        // Vérifier que la description n'est pas vide
        if (empty($description)) {
            throw new Exception("La description ne peut pas être vide");
        }

        // Assigner la nouvelle valeur
        $this->_description = $description;
    }

    public function setDate(string $date)
    {
        // Vérifier que la date est au bon format
        $d = DateTime::createFromFormat('Y-m-d', $date);
        if ($d && $d->format('Y-m-d') === $date) {
            // Assigner la nouvelle valeur
            $this->_date = $date;
        } else {
            throw new Exception("La date n'est pas au format correct (Y-m-d)");
        }
    }

    public function setTitre(string $titre)
    {
        // Vérifier que le titre n'est pas vide
        if (empty($titre)) {
            throw new Exception("Le titre ne peut pas être vide");
        }

        // Assigner la nouvelle valeur
        $this->_titre = $titre;
    }

    public function setNomTmp(string $nomTmp)
    {
        // Vérifier que le nom temporaire n'est pas vide
        if (empty($nomTmp)) {
            throw new Exception("Le nom temporaire ne peut pas être vide");
        }

        // Assigner la nouvelle valeur
        $this->_nomTmp = $nomTmp;
    }

    /*---------------------------------------------------Hydrate---------------------------------------------------*/
    public function hydrate()
    {
        if (isset($donnees['ID'])) {
            $this->_id = $donnees['ID'];
        }

        if (isset($donnees['Adresse_Mail'])) {
            $this->_adresseMail = $donnees['Adresse_Mail'];
        }

        if (isset($donnees['Description'])) {
            $this->_description = $donnees['Description'];
        }

        if (isset($donnees['Date'])) {
            $this->_date = $donnees['Date'];
        }

        if (isset($donnees['Titre'])) {
            $this->_titre = $donnees['Titre'];
        }

        if (isset($donnees['Nom_Temporaire'])) {
            $this->_nomTmp = $donnees['Nom_Temporaire'];
        }
    }
}

?>