<?php

use LDAP\Result;


function existe_utilisateur ($pseudo, $mail) {
    require("application/modeles/connect.php");
    $dbHandler = connect();
    $sql = "SELECT pseudo_utilisateur, mail_utilisateur
            FROM utilisateur
            WHERE pseudo_utilisateur = ? OR mail_utilisateur = ?";
    $stHandle = $dbHandler->prepare($sql);
    $stHandle->execute(array($pseudo, $mail));
    $results = $stHandle->fetch(PDO::FETCH_ASSOC);
    if ($results == "") { //si pas de résultat alors cet utilisateur n'existe pas
        return false;
    }
    return true; //sinon il existe
}

function inscription_utilisateur ($pseudo, $mail, $password) {
    require("application/modeles/connect.php");
    $dbHandler = connect();
    $sql = "INSERT INTO utilisateur VALUES('$pseudo', '$mail', '$password')";
    $stHandle = $dbHandler->prepare($sql);
    $stHandle->execute();
}


function existe_utilisateur2($pseudo, $password) {
    require("application/modeles/connect.php");
    $dbHandler = connect();
    $sql = "SELECT pseudo_utilisateur, mdp_utilisateur 
    FROM utilisateur
    WHERE pseudo_utilisateur = ? AND mdp_utilisateur = ?";
    $stHandle = $dbHandler->prepare($sql);
    $stHandle->execute(array($pseudo, $password));
    $results = $stHandle->fetch(PDO::FETCH_ASSOC);
    if ($results == "") {
        return false; 
    
    }
    return True;
}

?>