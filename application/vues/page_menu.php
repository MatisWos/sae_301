<?php
$title = "Menu";
$style = "menu";

// HEADER 1
ob_start();
?>
<svg id="SVG_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88.68 91.04">
    <title>Fichier 1</title>
    <g id="Calque_2" data-name="Calque 2">
        <g id="Calque_1-2" data-name="Calque 1">
            <path d="M0,45.4C0,20.38,19.74.08,44.05,0,68.54-.08,88.47,20.25,88.68,45.51c.2,25-19.93,45.63-44.48,45.53C19.33,90.93,0,70.93,0,45.4Zm83.26.43c.1-22.54-17.42-40.7-39.16-40.58C22.67,5.36,5.26,23.17,5.15,45.09c-.11,22.5,17,40.56,38.48,40.67C65.89,85.89,83.16,68.49,83.26,45.83Z" />
            <path d="M21.38,42.77H69.21c1.33,0,2.82-.38,4,.09a23.48,23.48,0,0,1,4.26,2.94c-1.41.81-2.8,2.3-4.21,2.32-15.64.17-31.27.11-46.91.11H19.92c3.92,4,7.07,6.76,9.73,10,1.08,1.31,1.09,3.49,1.58,5.28-1.7-.46-3.86-.41-5-1.47-4.67-4.25-9-8.89-13.52-13.29-2.3-2.23-2.21-4.09.08-6.3,4.67-4.5,9.1-9.25,13.86-13.65,1-.92,3-.74,4.56-1.07-.28,1.53,0,3.53-.92,4.52-3,3.3-6.34,6.24-9.56,9.33Z" />
        </g>
    </g>
</svg>
<?php
$header_1 = ob_get_clean();

// HEADER 2
ob_start();
?>

<?php
$header_2 = ob_get_clean();

// HEADER 3
ob_start();
?>

<?php
$header_3 = ob_get_clean();

// HEADER 4
ob_start();
?>

<?php
$header_4 = ob_get_clean();

// CONTENT
ob_start();
// Récupérer les données du formulaire
// $titre = $_POST["titre"];
// $source = $_POST["source"];
// $texte = $_POST["texte"];

// // Afficher les informations
// echo "<h3>$titre</h3>";
// echo "<img src='$source'>";
// echo "<p>$texte</p>";
?>

<section>

    <img src="public/medias/Pizza.jpg" alt="Pizza">
    <h1>Menu Pizza</h1>
    <p>
        <u>Pizza Margherita</u> :
        <br>
        Une base de sauce tomate fraîche, garnie de mozzarella fondante, de feuilles de basilic parfumées, le tout sur une pâte à pizza croustillante et légèrement dorée. Simple mais délicieuse.
        <br>
        La pizza est servi avec une <b>boisson</b> de votre choix lors de la récupération de la commande.
        <br>
        Enfin, pour être toujours dans le thème de l'Italie, le dessert qui vous est proposé est un <b>tiramisu</b>.
    </p>

    <br>
    <p><b>16€</b></p>
    <br>
    <h2>Supplément</h2>
    <br>
</section>

<form action="index.php?route=page_menu"  method="POST" enctype="multipart/form-data">
        <label for="ingredient1">Ingredient 1</label>
        <input type="checkbox" id="ingredient1" name="ingredient1"/>
        <hr class="hr-separator">
        <label for="ingredient2">Ingredient 2</label>
        <input type="checkbox" id="ingredient2" name="ingredient2"/>
        <hr class="hr-separator">
        <label for="ingredient3">Ingredient 3</label>
        <input type="checkbox" id="ingredient3" name="ingredient3"/>
        <hr class="hr-separator">
        <label for="ingredient4">Ingredient 4</label>
        <input type="checkbox" id="ingredient4" name="ingredient4"/>
        <br>

    <!-- <input type="submit" name="action" value="Ajouter au panier"> -->
</form>

<form action="index.php?"  method="POST">

    <label for="menu">Menu   
        <input type="checkbox" id="menu" name="menu"/>
    </label>  
    <input type="submit" value="Ajouter au panier">
</form>
<?php
$content = ob_get_clean();
require("application/vues/template.php");
?>