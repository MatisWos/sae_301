<?php
$title = "Inscription";
$style = "formulaire";
ob_start();
?>

<form action="index.php?route=inscription" method="post">

    <label for="pseudo">Pseudo :</label>
    <input type="text" name="pseudo" id="pseudo" required>
    
    <label for="mail">Adresse mail :</label>
    <input type="text" name="mail" id="mail" required>

    <label for="password1">Mot de passe :</label>
    <input type="password" name="password1" id="password1" required>

    <label for="password2">Confirmation mot de passe :</label>
    <input type="password" name="password2" id="password2" required>

    <input type="submit" value="S'inscrire">

</form>

<?php 
$content = ob_get_clean();
require("application/vues/template.php");
?>