<?php 
$title = "Connexion";
$style = "formulaire";
ob_start();
?>

<form action="index.php?route=connexion" method="post">

    <label for="pseudo">Pseudo :</label>
    <input type="text" name=pseudo id="pseudo" required>
    
    <label for="password">Mot de passe :</label>
    <input type="password" name=password id="password" required>
    
    <input type="submit" name="connexion" id="connexion" value="Se connecter">

</form>

<?php 
$content = ob_get_clean();
require("application/vues/template.php")
?>