<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ?></title>
    
    <link rel="stylesheet" href="public/style/template.css">
    <link rel="stylesheet" href="public/style/<?= $style ?>.css">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.2/dist/leaflet.css" integrity="sha256-sA+zWATbFveLLNqWO2gtiw3HL/lh1giY/Inf1BJ0z14=" crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.9.2/dist/leaflet.js" integrity="sha256-o9N1jGDZrf5tS+Ft4gbIK7mYMipq9lqpVJ91xHSyKhg=" crossorigin="" defer></script>


    <script src="public/script/jquery-3.6.1.js" defer></script>
    <script src="public/script/<?= $style ?>.js" defer></script>
</head>

<body>
    <header>
        <div>
            <?= $header_1 ?>
        </div>
        <?= $header_2 ?>
        <?php
        $adresseMailSpe = "dominique.legoat@gmail.com";
        if (@$_SESSION['adresseMail'] == $adresseMailSpe) {
        ?>
            <?= $header_3 ?>
        <?php
        }
        ?>

        <?php
        if (!empty($_SESSION['menu'])) {
        ?>
            <?= $header_4 ?>
        <?php
        }
        ?>


    </header>

    <main>
        <?= $content ?>

        <?php
        if (isset($_SESSION["error"])) {
            echo $_SESSION["error"];
            echo $_SESSION["error"] = null;
        }
        ?>



    </main>
</body>

</html>